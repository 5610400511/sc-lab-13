package test2;

public class AddRunnable implements Runnable{
	private Contact con;
	private int count;
	private String n;
	private static int DELAY = 1;
	public AddRunnable(Contact con,int count,String n) {
		this.con = con;
		this.count = count;
		this.n = n;
		
	}
	
	@Override
	public void run() {
		try{
			for(int i=1;i<=count;i++){
				con.add(n);
				Thread.sleep(DELAY);
			}
		}catch(InterruptedException e){
			System.err.println("Error");
		}
	}

}
